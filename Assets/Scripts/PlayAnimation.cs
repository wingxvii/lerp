﻿using UnityEngine;

public class PlayAnimation : MonoBehaviour {

    public Animator anim;

    //plays animation
    public void Play(string clip) {
        anim.Play(clip);
    }



}
