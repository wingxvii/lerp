﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Listener : MonoBehaviour {
    public GameObject splash;
    public GameObject main;
    public GameObject sub1;
    public GameObject sub2;
    public GameObject sub3;

    public Vector3 splashPosition;
    public Vector3 mainPosition;
    public Vector3 sub1Position;
    public Vector3 sub2Position;
    public Vector3 sub3Position;

    public Vector2 splashSize;
    public Vector2 mainSize;
    public Vector2 sub1Size;
    public Vector2 sub2Size;
    public Vector2 sub3Size;

    public float splashRotation;
    public float mainRotation;
    public float sub1Rotation;
    public float sub2Rotation;
    public float sub3Rotation;

    //speed for splash to spin, decrease size and leave screen
    public float splashTransitionSpeed = 1;
    //time delay before splash leaves screen
    public float splashTransitionWait = 2;
    public float mainTransitionSpeed = 1;
    public float sub1TransitionSpeed = 1;
    public float sub2TransitionSpeed = 1;
    public float sub3TransitionSpeed = 1;

    public GameObject splashText;

    void Start()
    {
        //initalize position for lerp
        splashPosition = splash.transform.position;
        mainPosition = main.transform.position;
        sub1Position = sub1.transform.position;
        sub2Position = sub2.transform.position;
        sub3Position = sub3.transform.position;

        //initalize size for lerp
        splashSize = splash.GetComponent<RectTransform>().sizeDelta;
        mainSize = main.GetComponent<RectTransform>().sizeDelta;
        sub1Size = sub1.GetComponent<RectTransform>().sizeDelta;
        sub2Size = sub2.GetComponent<RectTransform>().sizeDelta;
        sub3Size = sub3.GetComponent<RectTransform>().sizeDelta;

        //initalize rotation for lerp
        splashRotation = splash.transform.rotation.z;
        mainRotation = main.transform.rotation.z;
        sub1Rotation = sub1.transform.rotation.z;
        sub2Rotation = sub2.transform.rotation.z;
        sub3Rotation = sub3.transform.rotation.z;
    }

    void Update()
    {

        //space to hide splash
        if (Input.GetKeyDown(KeyCode.Space)) {
            splashSize = new Vector2(104.5f, 58.8f);
            splashRotation = 360f;
        }

        //trigger second animation
        if (splash.GetComponent<RectTransform>().sizeDelta.x < 105)
        {
            StartCoroutine(WaitSplash());
        }
        
        //update position with lerp
        splash.transform.position = Vector3.Lerp(splash.transform.position, splashPosition, Time.deltaTime * splashTransitionSpeed);
        main.transform.position = Vector3.Lerp(main.transform.position, mainPosition, Time.deltaTime * mainTransitionSpeed);
        sub1.transform.position = Vector3.Lerp(sub1.transform.position, sub1Position, Time.deltaTime * sub1TransitionSpeed);
        sub2.transform.position = Vector3.Lerp(sub2.transform.position, sub2Position, Time.deltaTime * sub2TransitionSpeed);
        sub3.transform.position = Vector3.Lerp(sub3.transform.position, sub3Position, Time.deltaTime * sub3TransitionSpeed);

        //update size for lerp
        splash.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(splash.GetComponent<RectTransform>().sizeDelta, splashSize, Time.deltaTime*splashTransitionSpeed);
        main.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(main.GetComponent<RectTransform>().sizeDelta, mainSize, Time.deltaTime * mainTransitionSpeed);
        sub1.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(sub1.GetComponent<RectTransform>().sizeDelta, sub1Size, Time.deltaTime * sub1TransitionSpeed);
        sub2.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(sub2.GetComponent<RectTransform>().sizeDelta, sub2Size, Time.deltaTime * sub2TransitionSpeed);
        sub3.GetComponent<RectTransform>().sizeDelta = Vector2.Lerp(sub3.GetComponent<RectTransform>().sizeDelta, sub3Size, Time.deltaTime * sub3TransitionSpeed);

        //update rotation for lerp
        splash.transform.eulerAngles = new Vector3(splash.transform.eulerAngles.x, splash.transform.eulerAngles.y, Mathf.Lerp(splash.transform.eulerAngles.z, splashRotation, Time.deltaTime * splashTransitionSpeed));
        main.transform.eulerAngles = new Vector3(main.transform.eulerAngles.x, main.transform.eulerAngles.y, Mathf.Lerp(main.transform.eulerAngles.z, mainRotation, Time.deltaTime * mainTransitionSpeed));
        sub1.transform.eulerAngles = new Vector3(sub1.transform.eulerAngles.x, sub1.transform.eulerAngles.y, Mathf.Lerp(sub1.transform.eulerAngles.z, sub1Rotation, Time.deltaTime * sub1TransitionSpeed));
        sub2.transform.eulerAngles = new Vector3(sub2.transform.eulerAngles.x, sub2.transform.eulerAngles.y, Mathf.Lerp(sub2.transform.eulerAngles.z, sub2Rotation, Time.deltaTime * sub2TransitionSpeed));
        sub3.transform.eulerAngles = new Vector3(sub3.transform.eulerAngles.x, sub3.transform.eulerAngles.y, Mathf.Lerp(sub3.transform.eulerAngles.z, sub3Rotation, Time.deltaTime * sub3TransitionSpeed));

        //update text size to scale to parent
        splashText.transform.localScale = new Vector3(splash.GetComponent<RectTransform>().sizeDelta.x/1045 , splash.GetComponent<RectTransform>().sizeDelta.y/588);
    }

    //used to delay splash transition
    private IEnumerator WaitSplash()
    {
        //stops rotation
        splashRotation = splash.transform.eulerAngles.z;
        //waits
        yield return new WaitForSeconds(splashTransitionWait);
        //moves splash off screen
        splashPosition.x = -1045;
    }


}
